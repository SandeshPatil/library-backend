import { type Response } from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import User, { type IUser, type UserModel } from "../models/user";
import ResponseHelper from "../helpers/responseHelper";

import { config } from "../config/config";

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class UserService {
  public static async register(res: Response, userData: IUser): Promise<void> {
    try {
      const userObj = new User({
        ...userData,
      });

      await userObj.save();

      ResponseHelper.created(res, undefined, "User Registered");
    } catch (error) {
      ResponseHelper.internalServerError(res, error);
    }
  }

  public static async login(
    res: Response,
    email: string,
    password: string
  ): Promise<void> {
    try {
      const user = (await User.findOne({ email })) as UserModel;
      if (!user) {
        ResponseHelper.notFound(res, "User not found");
        return;
      }

      const result = await bcrypt.compare(password, user.password);
      if (!result) {
        ResponseHelper.badRequest(res, "Invalid Password");
        return;
      }

      const token = jwt.sign(
        { id: user._id, role: user.role },
        config.JWTSECRET // Make sure 'config.SECRET' is defined and imported from a configuration file
      );

      ResponseHelper.success(
        res,
        { token, user: { name: user.username, role: user.role } },
        "Login successful"
      );
    } catch (err) {
      ResponseHelper.internalServerError(res, err);
    }
  }

  public static async getProfile(res: Response, userId: string): Promise<void> {
    try {
      const user = (await User.findOne({ _id: userId })) as UserModel;
      if (!user) {
        ResponseHelper.notFound(res, "User not found");
        return;
      }

      ResponseHelper.success(res, { user }, "User Found");
    } catch (error) {
      ResponseHelper.internalServerError(res, error);
    }
  }
}
