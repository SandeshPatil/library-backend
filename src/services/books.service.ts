import mongoose from "mongoose";
import Book from "../models/book";

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class BookService {
  public static async createBook(
    bookName: string,
    bookAuthor: string,
    bookGenres: string[],
    published: Date
  ): Promise<mongoose.Document> {
    const existingBook = await Book.findOne({ bookName, bookAuthor });
    if (existingBook) {
      throw new Error(
        `This book '${bookName}' by '${bookAuthor}' already exists`
      );
    }

    const book = new Book({
      _id: new mongoose.Types.ObjectId(),
      bookName,
      bookAuthor,
      bookGenres,
      published,
    });
    console.log("🚀 ~ file: books.service.ts:26 ~ BookService ~ book:", book);
    await book.save();
    return book;
  }

  public static async readBook(bookId: string): Promise<mongoose.Document> {
    // const authorId = req.params.authorid;

    const book = await Book.findById(bookId);
    if (!book) {
      throw new Error("Book not found");
    }
    return book;
  }

  public static async readAllBooks(): Promise<mongoose.Document[]> {
    const books = await Book.find();
    return books;
  }

  public static async updateBook(
    bookId: string,
    bookName: string,
    bookAuthor: string,
    bookGenres: string[],
    published: string
  ): Promise<mongoose.Document | null> {
    let book = await Book.findById(bookId);

    if (!book) {
      return null;
    }

    book = await Book.findOneAndUpdate(
      { _id: new mongoose.Types.ObjectId(bookId) },
      { bookName, bookAuthor, bookGenres, published },
      { new: true }
    );

    return book;
  }

  // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions

  public static async deleteBook(bookId: string): Promise<mongoose.Document> {
    const book = await Book.findByIdAndDelete(bookId);
    if (!book) {
      throw new Error("Book not found");
    }
    return book;
  }
}
