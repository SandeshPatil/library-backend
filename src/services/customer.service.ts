import mongoose, { Types } from "mongoose";
import Customer, { type UpdateCustomerInterface } from "../models/customer";
import Book from "../models/book";

interface CustomerData {
  firstName: string;
  lastName: string;
  email: string;
  subscriptionDate: string;
  subscriptionExpiry: string;
}

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class CustomerService {
  public static async createCustomer(
    customerData: CustomerData
  ): Promise<mongoose.Document> {
    const { email } = customerData;

    // Check if customer with the same email already exists
    const existingCustomer = await Customer.findOne({ email });
    if (existingCustomer) {
      throw new Error("Customer with this email already exists");
    }

    const customer = new Customer({
      ...customerData,
    });

    try {
      await customer.save();
      return customer;
    } catch (error) {
      throw new Error; 
    }
  }

  public static async readCustomer(
    customerId: string
  ): Promise<mongoose.Document> {
    const customer = await Customer.findById(customerId);

    if (!customer) {
      throw new Error("Customer not found");
    }
    return customer;
  }

  public static async readAllCustomers(): Promise<any> {
    try {
      const customers = await Customer.find()
        .populate({
          path: "currentBorrowed",
          model: "Book",
          select: "_id bookName",
        })
        .exec();
      console.log(customers,"Get Customer");
      
      return customers;
      
      
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  public static async updateCustomer(
    customerId: string,
    body: UpdateCustomerInterface
  ): Promise<mongoose.Document | null> {
    let customer = await Customer.findById(customerId);

    if (!customer) {
      throw new Error("Customer Not Found");
    }

    customer = await Customer.findOneAndUpdate(
      { _id: new mongoose.Types.ObjectId(customerId) },
      {
        firstName: body.firstName,
        lastName: body.lastName,
        email: body.email,
        subscriptionDate: body.subscriptionDate,
        subscriptionExpiry: body.subscriptionExpiry,
      },
      { new: true }
    );
    return customer;
  }

  public static async deleteCustomer(
    customerId: string
  ): Promise<mongoose.Document> {
    const customer = await Customer.findByIdAndDelete(customerId);
    if (!customer) {
      throw new Error("Customer not found");
    }
    return customer;
  }
  
public static async borrowBooks(
    customerId: string,
    bookIds: string[],
    date: Date
  ): Promise<mongoose.Document> {
    let customer = await Customer.findById(customerId);
    if (!customer) {
      throw new Error("Customer not found");
    }
    // Function to convert bookIds to Mongoose ObjectIDs
    function toMongooseIds(
      bookIds: string | string[]
    ): mongoose.Types.ObjectId | mongoose.Types.ObjectId[] {
      if (Array.isArray(bookIds)) {
        return bookIds.map((id) => new mongoose.Types.ObjectId(id));
      }
      return new mongoose.Types.ObjectId(bookIds);
    }

    // Check if all bookIds exist and are valid
    const existingBooks = await Book.find({ _id: { $in: bookIds } });
    const validBookIds = existingBooks.map((book) => book._id.toString());

    const invalidBookIds = bookIds.filter(
      (bookId) => !validBookIds.includes(bookId)
    );

    if (invalidBookIds.length > 0) {
      throw new Error(`Invalid book IDs: ${invalidBookIds.join(", ")}`);
    }

    const newRecords = bookIds.map((bookId: string) => ({
      bookId,
      date,
      type: "Borrowed",
    }));
    // Convert bookIds to Mongoose ObjectIDs
    console.log("BookIds before to MongooseId:", bookIds);
    
    const mongooseBookIds = toMongooseIds(bookIds);
    console.log("After converting to mongoose objec id:", mongooseBookIds);
    

    customer = await Customer.findByIdAndUpdate(
      customerId,
      {
        $push: {
          currentBorrowed: { $each: mongooseBookIds },
          records: { $each: newRecords },
        },
      },
      { new: true }
    );

    if (!customer) {
      throw new Error("Customer could not be updated");
    }

    return customer;
  }

  public static async returnBooks(
    customerId: string,
    bookIds: string[],
    date: Date
  ): Promise<mongoose.Document> {
    // Convert bookIds to Mongoose ObjectIDs
    console.log('bookIds', bookIds);
    
    const mongooseBookIds = bookIds.map((id) => new Types.ObjectId(id));

    let customer = await Customer.findById(customerId);
    if (!customer) {
      throw new Error("Customer not found");
    }

    // Check if bookIds exist in records in the currentBorrowed array
    const booksToRemove = customer.currentBorrowed.filter((book) =>
      mongooseBookIds.some((mongooseId) => mongooseId.equals(book))
    );

    // Define newRecords based on the booksToRemove
    const newRecords = booksToRemove.map((bookId: Types.ObjectId) => ({
      bookId,
      date,
      type: "Returned",
    }));

    // Update customer by removing books from currentBorrowed and adding records
    customer = await Customer.findByIdAndUpdate(
      customerId,
      {
        $pull: { currentBorrowed: { $in: booksToRemove } },
        $push: { records: { $each: newRecords } },
      },
      { new: true } // To get the updated document
    );

    if (!customer) {
      throw new Error("Customer could not be updated");
    }
    return customer;
  }
}
