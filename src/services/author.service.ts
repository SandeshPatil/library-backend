import type mongoose from "mongoose";
import Author from "../models/author";

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class AuthorService {
  public static async createAuthor(
    firstName: string,
    lastName: string,
    books: string[]
  ): Promise<mongoose.Document> {
    const existingAuthor = await Author.findOne({ firstName, lastName, books });
    if (existingAuthor) {
      throw new Error(`This author '${firstName}${lastName} ' already exists`);
    }

    const author = new Author({
      firstName,
      lastName,
      books,
    });

    await author.save();
    return author;
  }

  public static async readAuthor(authorId: string): Promise<mongoose.Document> {
    const author = await Author.findById(authorId);

    if (!author) {
      throw new Error("Author not found");
    }
    return author;
  }

  public static async readAuthors(): Promise<mongoose.Document[]> {
    const authors = await Author.find();
    return authors;
  }

  public static async updateAuthor(
    authorId: string,
    firstName: string,
    lastName: string,
    books: string[]
  ): Promise<mongoose.Document> {
    const author = await Author.findOneAndUpdate(
      { _id: authorId },

      { firstName, lastName, books },
      { new: true }
    );

    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!author) {
      throw new Error("Author not found");
    }
    return author;
  }

  public static async deleteAuthor(
    authorId: string
  ): Promise<mongoose.Document> {
    const author = await Author.findByIdAndDelete(authorId);

    if (!author) {
      throw new Error("Author not found");
    }
    return author;
  }
}
