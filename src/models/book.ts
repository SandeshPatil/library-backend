import mongoose from "mongoose";
const bookSchema = new mongoose.Schema(
  {
    bookName: {
      type: String,
      required: [true, "Please enter a book name."],
      // book: [{ type: Schema.Types.ObjectId, ref: "BookName" }],
    },
    bookAuthor: {
      type: String,
      required: [
        true,
        "Please enter the author name, otherwise who do we pay the royalties to? We can not have them starving!",
      ],
    },
    bookGenres: [
      {
        type: String,
        required: [
          true,
          "Please enter Genres, Helps us catalogue better and make it easy for you to search!",
        ],
      },
    ],
    published: {
      type: Date,
      required: [true, "Lets us understand whether it was ahead of its time! "],
    },
  },
  {
    timestamps: true,
  }
);

const Book = mongoose.model("Book", bookSchema);

export default Book;
