import mongoose, { type Document, Schema } from "mongoose";

export interface IAuthor {
  firstName: string;
  lastName: string;
  books: string[];
}

export interface IAuthorModel extends IAuthor, Document {}

const AuthorSchema: Schema = new Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  books: [
    {
      type: String,
      required: [true],
    },
  ],
});
const Author = mongoose.model("Author", AuthorSchema);

export default Author;
