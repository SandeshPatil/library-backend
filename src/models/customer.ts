import mongoose, { type Document, Schema, type Types } from "mongoose";


export interface Customer {
  firstName: string;

  lastName: string;

  email: string;

  subscriptionDate: string;

  subscriptionExpiry: string;

  records: Record[];

  currentBorrowed: Types.ObjectId[];
}
export type UpdateCustomerInterface = Omit<
  Customer,
  "records" | "currentBorrowed"
>;
interface Record {
  bookid: string;
  date: Date;
  type: string; // "Borrowed"/"Returned"
}

export interface CustomerModel extends Customer, Document {}

const customerSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  subscriptionDate: {
    type: String,
    required: true,
  },
  subscriptionExpiry: {
    type: String,
    required: true,
  },
  records: [
    {
      bookid: {
        type: String,
        required: true,
      },
      date: {
        type: Date,
        required: true,
      },
      type: {
        type: String,
        required: true,
      },
    },
  ],

  currentBorrowed: [
    {
      type: Schema.Types.ObjectId,
      ref: "Book", // Reference the "Book" model
    },
  ],
});

export default mongoose.model<CustomerModel>("Customer", customerSchema);
