import mongoose, { type Document } from "mongoose";
import bcrypt from "bcrypt";

export interface IUser {
  username: string;
  email: string;
  password: string;
  role: "admin" | "user" | undefined;
}

export interface IUserLogin {
  email: string;
  password: string;
}

export interface UserModel extends IUser, Document {
  comparePassword: (
    candidatePassword: string,
    cb: (err: any, isMatch?: boolean) => void
  ) => void;
}

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    // eslint-disable-next-line no-useless-escape
    match: /.+\@.+\..+/,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ["admin", "user"],
    default: "user",
  },
});

userSchema.pre<UserModel>("save", function (next) {
  const user = this;

  if (!user.isModified("password")) {
    next();
    return;
  }
  if (user.isModified("password")) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        next(err);
        return;
      }

      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) {
          next(err);
          return;
        }
        user.password = hash;
        next();
      });
    });
  }
});

userSchema.methods.comparePassword = function (
  candidatePassword: string,
  cb: (err: any, isMatch?: boolean) => void
) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) {
      cb(err);
      return;
    }
    cb(null, isMatch);
  });
};

const User = mongoose.model<UserModel>("User", userSchema);

export default User;
