import { type NextFunction, type Request, type Response } from "express";

import { BookService } from "../services/books.service";

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class BookController {
  public static async createBook(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const { bookName, bookAuthor, bookGenres, published } = req.body;
      
     

      const response = await BookService.createBook(
        bookName,
        bookAuthor,
        bookGenres,
        published
      );
      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  public static async readBook(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const bookId = req.params.bookId;
  
  

    try {
      res.send(await BookService.readBook(bookId));
    } catch (error) {
      next(error);
    }
  }

  // Read all books

  public static async readAllBooks(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const books = await BookService.readAllBooks();
      res.send(books);
    } catch (error) {
      next(error);
    }
  }

  //  update Book

  public static async updateBook(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const bookId = req.params.bookId;
    const { bookName, bookAuthor, bookGenres, published } = req.body;
   

    try {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const book = await BookService.updateBook(
        bookId,
        bookName,
        bookAuthor,
        bookGenres,
        published
      );
      res.send("Book Updated");
    } catch (error) {
      next(error);
      
    }
  }

  //  delete Book

  public static async deleteBook(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const bookId = req.params.bookId;

    try {
      res.send(await BookService.deleteBook(bookId));
    } catch (error) {
      next(error);
    }
  }
}
