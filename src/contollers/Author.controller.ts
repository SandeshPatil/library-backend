/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { type NextFunction, type Request, type Response } from "express";


import { AuthorService } from "../services/author.service";
//

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class AuthorController {
  public static async createAuthor(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {

    const { firstName, lastName, books } = req.body;

    try {
      const createdAuthor = await AuthorService.createAuthor(
        firstName,
        lastName,
        books
      );
      res.send(createdAuthor);
    } catch (error) {
      next(error);
    }
    // };
  }

  public static async readAuthor(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const authorId = req.params.authorId;
      try {
      const author = await AuthorService.readAuthor(authorId);
      res.send(author);
    } catch (error) {
      next(error);
    }
  }

  public static async readAuthors(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const authors = await AuthorService.readAuthors();
      res.send(authors);
    } catch (error) {
      next(error);
    }
  }

  public static async updateAuthor(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const authorId = req.params.authorId;
    const { firstName, lastName, books } = req.body;

    try {
      const author = await AuthorService.updateAuthor(
        authorId,
        firstName,
        lastName,
        books
      );
      res.send(author);
    } catch (error) {
      next(error);
    }
  }

  public static async deleteAuthor(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const authorId = req.params.authorId;
    try {
      res.send(await AuthorService.deleteAuthor(authorId));
    } catch (error) {
      next(error);
    }
  }
}
