import { type Request, type Response } from "express";

import { UserService } from "../services/user.service";

// models
import { type IUser, type IUserLogin } from "../models/user";

// helpers
import ResponseHelper from "../helpers/responseHelper";
import { validationResult } from "express-validator";

/**
 * Handle user registration
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const register = async (req: Request, res: Response) => {
  const { username, email, password, role }: IUser = req.body;
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.send();
    }

    await UserService.register(res, {
      username,
      email,
      password,
      role,
    });
  } catch (error: any) {
    ResponseHelper.internalServerError(res, error);
  }
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const login = async (req: Request, res: Response) => {
  const { email, password }: IUserLogin = req.body;
  try {
    await UserService.login(res, email, password);
  } catch (err) {
    ResponseHelper.internalServerError(res, err);
  }
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const profile = async (req: Request, res: Response) => {
  const { id } = req.body.user;
  try {
    await UserService.getProfile(res, id);
  } catch (error) {
    ResponseHelper.internalServerError(res, error);
  }
};
