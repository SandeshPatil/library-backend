import { type NextFunction, type Request, type Response } from "express";
import { CustomerService } from "../services/customer.service";

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class CustomerController {
  public static async createCustomer(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const { firstName, lastName, email, subscriptionDate, subscriptionExpiry } =
      req.body;
    const customer = {
      firstName,
      lastName,
      email,
      subscriptionDate,
      subscriptionExpiry,
    };
    try {
      res.send(await CustomerService.createCustomer(customer));
    } catch (error) {

      next(error);
    }
  }

  public static async readCustomer(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const customerId = req.params.customerId;

    try {
      const customers = await CustomerService.readCustomer(customerId);

      res.send(customers);
    } catch (error) {
      next(error);
    }
  }

  public static async readAllCustomers(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const customers = await CustomerService.readAllCustomers();

      res.send(customers);
    } catch (error) {
      next(error);
    }
  }

  public static async updateCustomer(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const customerId = req.params.customerId;
    const { firstName, lastName, email, subscriptionDate, subscriptionExpiry } =
      req.body;
    try {
      await CustomerService.updateCustomer(customerId, {
        firstName,
        lastName,
        email,
        subscriptionDate,
        subscriptionExpiry,
      });
      res.send("Customer Updated");
    } catch (error) {
      next(error);
    }
  }

  public static async deleteCustomer(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const customerId = req.params.customerId;
    try {
      res.send(await CustomerService.deleteCustomer(customerId));
    } catch (error) {
      next(error);
    }
  }

  public static async borrowBooks(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const customerId = req.params.customerId;
    const { bookIds, date } = req.body;
    

    try {
      const customer = await CustomerService.borrowBooks(
        customerId,
        bookIds,
        date
      );
      res
        .status(200)
        .json({ message: "Books borrowed successfully", customer });
    } catch (error) {
      res.status(500).json({
        message: "An error occurred",
        error: (error as Error).message,
      });
    }
  }

  public static async returnBooks(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const customerId = req.params.customerId;

    const { bookIds, date } = req.body;

    try {
      const customer = await CustomerService.returnBooks(
        customerId,
        bookIds,
        date
      );
      

      res.status(200).send(customer);
      
    } catch (error) {
      next(error);
    }
  }
}
