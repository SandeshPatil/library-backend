import dotenv from "dotenv";

dotenv.config();

const MONGO_URL =
  "mongodb+srv://pushkarbhosale09:Test123@cluster1.mt4mkyr.mongodb.net/";
const SERVER_PORT =
  process.env.SERVER_PORT != null ? Number(process.env.SERVER_PORT) : 1337;

export const config = {
  mongo: {
    url: MONGO_URL,
  },
  server: { port: SERVER_PORT },

  SECRET: "@01@#$",

  JWTSECRET: "@JWTSECRET",
};
