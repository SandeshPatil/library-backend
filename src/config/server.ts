import { config } from "./config";

import mongoose from "mongoose";

import AuthorRouter from "../routes/author";
import BookRouter from "../routes/book";
import CustomerRouter from "../routes/customer";
import cors from "cors";
import UserRegisterRouter from "../routes/userRegister";
import UserSignInRouter from "../routes/userLogin";
import express from "express";



/** Connect to Mongo */

const customRouter = express.Router();

mongoose
  .connect(config.mongo.url)
  .then(() => {
    console.log("Connected to Mongodb");
    
  })
  .catch((err) => {
    console.log(err);
    console.log(config.mongo.url);
    console.log("error connecting to the database");
  });
customRouter.use(express.urlencoded({ extended: true }));
customRouter.use(express.json);

/** rules of APIs */
customRouter.use((req, res, next) => {
  res.header("Acsess-Control-Allow-Origin", "*");
  res.header(
    "Acsess-Control-Allow-Headers",
    "origin, X-Requested-With, Content-Type, Accept, Authorisation"
  );

  if (req.method === "OPTIONS") {
    res.header("Access_Control-Allow-Methods", "PUT, POST, PATCH, DELETE,GET");
    return res.status(200).json({});
  }

  next();
});

/** Healthcheck */
customRouter.get("/ping", (req, res, next) =>
  res.status(200).json({ message: "pong" })
);

const app = express();
app.use(express.json());
app.listen(6500, () => {
  console.log("Listening on 6500");
});
app.use(cors());

app.get("/ping", (req, res, next) => res.status(200).json({ message: "pong" }));

app.use(
  "/author",
  (req, res, next) => {
    next();
  },
  AuthorRouter
);
app.use("/books", BookRouter);

app.use(
  "/customers",
  (req, res, next) => {
    next();
  },
  CustomerRouter
);

app.use("/userSignIn", UserSignInRouter);

app.use("/userRegister", UserRegisterRouter);
