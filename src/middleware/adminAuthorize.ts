import { type Request, type Response, type NextFunction } from "express";

// helpers

import ResponseHelper from "../helpers/responseHelper";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const adminAuthorise = (req: Request, res: Response, next: NextFunction) => {
  try {
    const { role } = req.body.user;
    if (role !== "admin") {
      throw new Error();
    }
  } catch (error) {
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    return ResponseHelper.unauthorized(res, "You don't have permission!");
  }
};

export default adminAuthorise;
