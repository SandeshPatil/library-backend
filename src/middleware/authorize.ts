import { type Request, type Response, type NextFunction } from "express";
import jwt, { type Jwt } from "jsonwebtoken";
import { config } from "../config/config";

// helpers
import ResponseHelper from "../helpers/responseHelper";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const authorize = (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization?.split(" ")[1];
    if (!token) {
      // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
      return ResponseHelper.unauthorized(res, "Unauthorized");
    }

    const decode = jwt.verify(token, config.SECRET) as Jwt;
    req.body.user = decode;

    next();
  } catch (error) {
    // eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
    return ResponseHelper.unauthorized(res, "Invalid Request!");
  }
};

export default authorize;
