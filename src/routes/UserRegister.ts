import { Router } from "express";

// controllers
import { register } from "../contollers/user.controller";
const UserRegisterRouter = Router();

UserRegisterRouter.post("/user/", register);

export default UserRegisterRouter;
