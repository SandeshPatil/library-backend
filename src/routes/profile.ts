import { Router } from "express";

// controllers
import { profile } from "../contollers/user.controller";

// middlewares
import authorize from "../middleware/authorize";
const ProfileRouter = Router();

ProfileRouter.get("/users", authorize, profile);

export default ProfileRouter;
