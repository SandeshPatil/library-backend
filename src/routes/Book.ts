import express from "express";
import { BookController } from "../contollers/book.contoller";

export const BookRouter = express.Router();
// eslint-disable-next-line @typescript-eslint/no-misused-promises
BookRouter.post("/create", BookController.createBook);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
BookRouter.get("/get", BookController.readAllBooks);
BookRouter.get("/get/:bookId", BookController.readBook);
// eslint-disable-next-line @typescript-eslint/no-misused-promises

// eslint-disable-next-line @typescript-eslint/no-misused-promises
BookRouter.patch("/update/:bookId", BookController.updateBook);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
BookRouter.delete("/delete/:bookId", BookController.deleteBook);

export default BookRouter;
