import express from "express";
import { CustomerController } from "../contollers/customer.controller";

export const CustomerRouter = express.Router();
// eslint-disable-next-line @typescript-eslint/no-misused-promises
CustomerRouter.post("/create/", CustomerController.createCustomer);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
CustomerRouter.get("/get", CustomerController.readAllCustomers);
CustomerRouter.get(
  "/get/:customerId",
  (req, res, next) => {
    next();
  },
  CustomerController.readCustomer
);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
CustomerRouter.get("/get/", CustomerController.readAllCustomers);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
CustomerRouter.patch("/:customerId/update", CustomerController.updateCustomer);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
CustomerRouter.delete(
  "/:customerId/delete/",
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  CustomerController.deleteCustomer
);
// eslint-disable-next-line @typescript-eslint/no-misused-promises

CustomerRouter.post(
  "/:customerId/borrowMany",
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  CustomerController.borrowBooks
);

CustomerRouter.post(
  "/:customerId/returnMany",
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  CustomerController.returnBooks
);

export default CustomerRouter;
