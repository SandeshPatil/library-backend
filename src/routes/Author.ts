import express from "express";
import { AuthorController } from "../contollers/author.controller";

export const AuthorRouter = express.Router();
// eslint-disable-next-line @typescript-eslint/no-misused-promises
AuthorRouter.post("/create", AuthorController.createAuthor);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
AuthorRouter.get("/get/", AuthorController.readAuthors);
AuthorRouter.get("/get/:authorId", AuthorController.readAuthor);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
AuthorRouter.patch("/update/:authorId", AuthorController.updateAuthor);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
// router.patch("/update/:authorId", AuthorController.UpdateAuthor);
// eslint-disable-next-line @typescript-eslint/no-misused-promises
AuthorRouter.delete("/delete/:authorId", AuthorController.deleteAuthor);

export default AuthorRouter;
