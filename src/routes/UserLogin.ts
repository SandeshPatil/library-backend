import { Router } from "express";

// controller
import { login } from "../contollers/user.controller";
const UserSignInRouter = Router();

UserSignInRouter.post("/user/", login);

export default UserSignInRouter;
