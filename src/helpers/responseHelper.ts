/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { type Response } from "express";

export interface ErrorResponse {
  message: string;
}

export enum StatusCode {
  Success = 200,
  Created = 201,
  BadRequest = 400,
  Unauthorized = 401,
  NotFound = 404,
  InternalServerError = 500,
}

export interface ApiResponse<T> {
  status: StatusCode;
  success: boolean;
  message?: string;
  data?: T;
}

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export default class ResponseHelper {
  static response<T>(
    res: Response,
    code: StatusCode,
    success: boolean,
    message?: string,
    data?: T
  ) {
    const response: ApiResponse<T> = {
      status: code,
      success,
    };

    if (message) {
      response.message = message;
    }

    if (data !== undefined) {
      response.data = data;
    }

    res.status(code).json(response);
  }

  static success<T>(res: Response, data?: T, message?: string) {
    this.response(res, StatusCode.Success, true, message, data);
  }

  static created<T>(res: Response, data?: T, message?: string) {
    this.response(res, StatusCode.Created, true, message, data);
  }

  static badRequest(res: Response, message?: string) {
    this.response(res, StatusCode.BadRequest, false, message);
  }

  static unauthorized(res: Response, message?: string) {
    this.response(res, StatusCode.Unauthorized, false, message);
  }

  static notFound(res: Response, message?: string) {
    this.response(res, StatusCode.NotFound, false, message);
  }

  static internalServerError(res: Response, error: any) {
    this.response<ErrorResponse>(
      res,
      StatusCode.InternalServerError,
      false,
      error.message
    );
  }
}
